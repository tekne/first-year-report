\documentclass[11pt]{report}

% Layout
\usepackage{setspace}
\usepackage[margin=3cm]{geometry}

% Imports
\usepackage{isotope}

% Bibliography resources
\addbibresource{references.bib}

% Spacing (for submission version)
% \renewcommand{\baselinestretch}{2.0}

\title{Dependent Types with Borrowing}
\author{Jad Elkhaleq Ghalayini}
\date{Trinity Term, 2021}

\begin{document}

\pagenumbering{gobble} 

\begin{center}

    {\Huge PhD Proposal}


    \vspace{4cm}

    \includegraphics{uc-black-white.eps}

    \vspace{3cm}

    {\Large Jad Elkhaleq Ghalayini}


    \bigskip
    Darwin College


    \bigskip
    Supervised by Neel Krishnaswami


    \vspace{3cm}

    University of Cambridge \\
    Department of Computer Science and Technology \\
    William Gates Building \\
    15 JJ Thomson Avenue \\
    Cambridge CB3 0FD \\
    {\sc United Kingdom}

    \bigskip

    Email: jeg74@cl.cam.ac.uk \\
    \TODO{insert date here}

\end{center}

\newpage

\tableofcontents

\newpage
\pagenumbering{arabic}

\chapter{Introduction}

\TODO{this}

\chapter{Background}

\section{Refinement Types}

Consider a simply-typed functional programming language with total functions. We can build a denotational semantics for this language by mapping every type to a set in the obvious way, for example, by writing 
\begin{equation}
    \dnt{A \times B} = \dnt{A} \times \dnt{B}, \qquad
    \dnt{A + B} = \dnt{A} \sqcup \dnt{B}, \qquad
    \dnt{A \to B} = \dnt{A} \to \dnt{B}
\end{equation}
If our language has side effects, such as nontermination, we may use a monadic encoding 
\begin{equation}
    \mc{E}\dnt{A} = \ms{M}\dnt{A}, \qquad \dnt{A \to B} = \dnt{A} \to \ms{M}\dnt{B}
\end{equation}
where \(\ms{M}\) is a monad representing our chosen effects. In many cases, however, the valid inputs for a function are not the entire input type but rather a subset of the input; for example, consider the classic \tty{head} function 
\begin{equation}
    \tty{head :: List A \(\to\) A}
\end{equation}
Note that we cannot define this as a total function for an arbitrary type \(A\); we must either:
\begin{itemize}
    \item Provide a default value, as in 
    \begin{equation}
        \begin{array}{l}
        \tty{head :: List A \(\to\) A \(\to\) A} \\
        \tty{head}\;[]\;a = a \\
        \tty{head}\;(h::x)\;\_ = h 
        \end{array}
    \end{equation}
    \item Change the return type, as in 
    \begin{equation}
        \begin{array}{l}
        \tty{head :: List A \(\to\) Option A} \\
        \tty{head}\;[] = \tty{None}\\
        \tty{head}\;(h::x) = \tty{Some}\;h 
        \end{array}
    \end{equation}
    \item Use some system of exceptions/partial functions, as in 
    \begin{equation}
        \begin{array}{l}
        \tty{head :: List A \(\to\) A} \\
        \tty{head}\;[] = \tty{error "head of empty list is not defined"}\\
        \tty{head}\;(h::x) = h
        \end{array}
    \end{equation}
\end{itemize}
Of course, for optimization purposes, it may be desirable to mark invalid calls as undefined behavior (UB) in the C sense, e.g., writing  
\begin{equation}
    \begin{array}{l}
    \tty{unsafeHead :: List A \(\to\) A} \\
    \tty{unsafeHead}\;[] = \tty{undef}\\
    \tty{unsafeHead}\;(h::x) = h
    \end{array}
\end{equation}
The issue is, of course, that this approach is highly unsafe. While this allows very aggressive optimizations (e.g., eliding branches), it also means that a single programming error may corrupt the entire environment in subtle and unpredictable ways, as in C. One way to get around these issues is through using types that guarantee some invariant about their inhabitants; for example, in a language with a \tty{NonEmptyList} type, we could write
\begin{equation}
    \begin{array}{l}
    \tty{head :: NonEmptyList A \(\to\) A}\\
    \tty{-- No empty pattern needed, since a NonEmptyList can never be empty!}\\
    \tty{head}\;(h::x) = h\\
    \end{array}
\end{equation}
While this also changes the semantics of the function, the type signature is arguably closer to the actual intention of the programmer. Furthermore, the same undefined behavior optimizations can still (safely) be applied. Indeed, the user can often implement the \tty{NonEmptyList} example using only ``safe" (language-checked) code, e.g., as follows:
\begin{equation}
    \begin{array}{l}
        \tty{data NonEmptyList A = Cons A (List A)} \\
        \tty{head}\;(\tty{Cons}\;h\;x) = h
    \end{array}
\end{equation}

It is in general a common pattern in many programming languages to uphold the invariants on members of a type through the use of private methods and constructors; for example, we could define a \tty{NonZero} type as follows:
\begin{equation}
    \begin{array}{l}
        \tty{data NonZero = NonZero Int -- We assume this constructor is private} \\
        \tty{mk :: Int \(\to\) Option NonZero} \\
        \tty{mk}\;0 = \tty{None} \\
        \tty{mk}\;(n + 1) = \tty{Some}\;(n + 1)
    \end{array}
\end{equation}
We can then encapsulate potentially unsafe functions, such as division, as follows
\begin{equation}
    \begin{array}{l}
        \tty{div :: Int \(\to\) NonZero \(\to\) Option NonZero} \\
        \tty{div}\;n\;(\tty{NonZero}\;m) = \tty{unsafeDiv}\;n\;m \qquad
        \tty{-- This is always safe!}
    \end{array}
\end{equation}
It is easy to see how we can do much more complex validation, such as, for example, checking that a \tty{String} type contains only valid UTF-8 text. However, while in general extremely useful, this approach has numerous drawbacks: for one, not only do runtime checks impose a performance penalty, but they often either force the user to think about bubbling up errors or, in the presence of exceptions/panics, can have similar drawbacks to simply writing the function itself using exceptions in the first place. Furthermore, while this pattern is often used in safe languages like Rust and Haskell to encapsulate unsafe code in a safe API, there is no guarantee that the API provided is sound.

Another limitation of this approach is that it often becomes exceptionally tedious and error-prone when one needs to check invariants unique to a given function. For example, consider the case of a function that reads the second element of a list, 
\begin{equation}
    \begin{array}{l}
        \tty{unsafeSnd :: List A \(\to\) A} \\
        \tty{unsafeSnd}\;[] = \tty{undef} \\
        \tty{unsafeSnd}\;(h::x) = \tty{unsafeHead}\;x
    \end{array}
\end{equation} 
We could define a new wrapper type \tty{Has2Elems} for this use case, but the likelihood of actually getting this as the return type of a function is quite remote, leading to code littered with casts and assertions. While we may be sure that every \tty{Has3Elems} is a valid \tty{Has2Elems}, we still have to write a function
\begin{equation}
    \tty{twoLtThree :: Has3Elems A \(\to\) Has2Elems A}
\end{equation}
representing this fact, or, alternatively, use a checked constructor, which, other than potentially caching the result of the validity check, does not provide much improvement over simply using a checked operation. The situation worsens when one has invariants that depend on runtime variables; for example, consider the pervasive operation of array indexing 
\begin{equation}
    \begin{array}{l}
        \tty{-- It is UB if the index provided is not in the range 0..len} \\
        \tty{unsafeIx :: Array A \(\to\) Int \(\to\) A}
    \end{array}
\end{equation}
While one can, with heroic effort, define and use a \tty{ValidIndex} type pairing a list and index into that list, it is far more maintainable and performant to use a checked index operation at this point. It is clear that a more scalable approach is required: one such approach is refinement typing.

Refinement typing, such as found in Liquid
Haskell~\cite{liquid-haskell}, allows us to refine types by annotating
them with a logical specification. From the types-as-sets perspective,
we assert that the values inhabiting a type satisfy a given logical
specification. We can naturally write all the above examples using
refinement typing: We can even handle more complicated cases, such as
matrix multiplication, which depends on runtime size constraints.
\TODO{this} The power of refinement typing comes from such
specifications automatically composing, for example, \TODO{matrix
  multiplication, and then indexing into the result}

\TODO{Verification conditions} The reason this kind of composition
works is essentially because of logical entailment: if the
postcondition of the first call implies the precondition of the second
call, then the composition will necessarily work. Note that this
verification is compositional: we check a composite expression in
terms of the \emph{types} of its subexpressions, and we never
re-examine a term's structure after it has been typechecked.

This means that the kinds of properties that can be expressed about
a program depend upon the logic used in the refinements. The richer
the logic, the more properties that can be expressed. However,
as we enrich a logic, it becomes progressively harder to solve
the entailment problem. 

Liquid Haskell tries to fully automate this entailment checking, and
as such has to restrict itself to a decidable fragment of logic.  In
particular, it restricts the expressible properties to those which can
be solved using ``satisfiability modulo theories''. These are
essentially quantifier-free SAT formulas, where the atoms are replaced
with formulas from decidable theories, such as linear arithmetic, the
theory of uninterpreted functions, the theory of datatypes, and so
on.

%TODO: SMT citation?

While SMT solving is decidable, it is based on a combination of
NP-hard problems, and can often exhibit unpredictable behaviour.
Worse still, the restriction to quantifier-free formulas can make it
difficult to express natural and important properties for
verification. For example, stating that an array is increasing
requires a universal quantifier:
\(\forall i, j. (i \leq j < |xs|) \implies a[i] \leq a[j]\).
Unfortunately, arithmetic formulae with quantifiers are generally
undecidable.

\TODO{this}

\TODO{erasure}

\section{Dependent Types}

\TODO{this}

\TODO{extraction}

\section{Extensionality}

\TODO{this}

\TODO{the essence of casting and the issues with equality}

\section{The RVSDG Intermediate Representation}

\TODO{this}

\TODO{heap manipulation and parallelism}

\chapter{The Isotope Project}

\section{Project Overview}

This project aims to develop a dependently typed intermediate representation for low-level programs with an associated set of optimizations and analyses. As this is a somewhat ambitious goal, we have split it into three components: 
\TODO{extend bullet points?}
\begin{itemize}
    \item Building a core dependently-typed language, \tty{isotope-core}.
    \item Building a functional, simply-typed intermediate representation for low-level programs, \tty{isotope-ir}. 
    \item Building a language, \tty{isotope}, of \tty{isotope-ir} programs refined using \tty{isotope-core} as logic. 
\end{itemize}
\tty{isotope-core} and \tty{isotope-ir} are independent projects which are currently being developed in parallel, while \tty{isotope} depends on both. Each of these projects can be further split into several sub-components and desired features, allowing the parallel development of several lines of research and opportunities for flexibility and changes in direction while still providing an overarching plan and theme.

\TODO{dependency diagram. Include sub-components?}

\TODO{rest}

\section{Isotope Core}

\TODO{Citation for MLTT?}
The \tty{isotope-core} language aims to implement a variant of extensional Martin-Löf type theory, which we will henceforth refer to simply as MLTT. We have the following design goals in mind:
\begin{itemize}
    \item Proofs should be serializable to a compact binary format, which can be type-checked rapidly by a small, trusted kernel. Type checking should be highly parallelizable.
    \item Terms that type-check intensionally should type-check extensionally without requiring tactics or macros to construct derivations (through the inference of appropriate beta-eta equalities). In particular, they should be able to internally use subterms that only type-check extensionally without breaking this property.
    \item There should be a straightforward, well-defined API for programmatically using the kernel, including saving, loading, and caching proofs.
    \item Terms should be content-addressed and hence trivially cachable.
    \item Terms should be able to be richly annotated with metadata, e.g., variable names and source location.
\end{itemize}
Additional design goals, which may require some extension of the underlying type theory, include:
\begin{itemize}
    \item Well-founded recursion should be a first-class language primitive.
    \item The language should be able to manipulate potentially non-terminating terms stored in monadic types. We plan to use extensionality to allow us to adapt the approach used in Zombie \cite{zombie}.
\end{itemize}
We currently plan to perform the implementation in Rust. Our current design splits the implementation of isotope-core into three parts:
\begin{itemize}
    \item A small kernel, the "nucleus," inspired by Andromeda \cite{andromeda}, provides an LCF-style \cite{paulson-lcf} API for constructing derivations. Naturally, the kernel is part of the trusted codebase.
    \item A (set of) reduction engine(s), which we also assume to be part of the trusted codebase. In particular, the kernel's API does not assume anything about the reduction engine used beyond that any terms it declares equal are indeed equal in MLTT. The reduction engine is then free to employ optimizations such as the congruence closure optimizations described in the author's WITS talk Typechecking up to Congruence, as well as other more exotic optimizations such as, e.g., beta-optimal reduction as in HVM, without adding complexity to the kernel.
    
    \TODO{Add notes about confluence?}

    \item A simple elaborator provides features such as inference of beta-eta equalities when type-checking terms. An error in the elaborator should never cause an invalid term to typecheck but may mean that a valid term does not type-check; hence, the elaborator is not part of the trusted codebase.
    
    \TODO{Explain that the elaborator is also run on the binary format, and hence can be considered part of the language but not the trusted codebase}
\end{itemize}
We also plan to implement a simple theorem-prover style surface language for manually interacting with the isotope-core kernel since Rust, being statically compiled, is unsuitable for a purely LCF-style interface. We may consider exposing a purely LCF-style interface using \tty{isotope-core}'s C bindings in another language.

\TODO{dependency diagram}

\TODO{text for features}

\begin{itemize}

    \item (Easy, Required) User-friendly parser, prettyprinter, and REPL

    \item (Easy, Required) Compact binary representation, separate metadata segment

    \item (Easy, Required) Congruence-closure-based equality management in the elaborator, and therefore in the surface language

    \item (Medium) Bidirectional type checking for surface language, perhaps some more advanced inference techniques

    \item (Medium) VSCode plugin

    \item (Medium) Converting FRAT \cite{frat}, LRAT \cite{lrat}, and DRAT \cite{drat} SAT proof certificates into \tty{isotope-core} terms for prover integration

    \item (Hard) Content-addressing and dependency-graph based caching for recompilation of edited sources

    \item (Hard) E-graph-based proof search in the surface language

    \item (Hard) Support for macros and tactics in the surface language

    \item (Very Hard) Converting SMT proof certificates into \tty{isotope-core} terms for prover integration. The theory of bitvectors and uninterpreted functions would be especially useful given the desired application.

\end{itemize}

\TODO{segue}

We have an experimental Rust implementation of a previous design for \tty{isotope-core}, which partially implements a (somewhat buggy) intensional variant of MLTT. This implementation aimed to experiment with separating the reduction engine from the kernel and the optimization and parallelization strategies for typechecking described in Typechecking up to Congruence. 

\section{Isotope IR}

\TODO{clearer naming?}

The \tty{isotope-ir} intermediate representation is a modified RVSDG designed to provide a functional representation of low-level programs. We have the following design goals in mind:
\begin{itemize}
    \item Since we represent programs as graphs, all dependencies between instructions, including state and memory dependencies, must be explicitly represented as edges.
    \item The intermediate representation should be structured similarly to a functional program; in particular, it should be (relatively) easy to provide it with denotational semantics.
    \item The intermediate representation should support explicit pointer-provenance integrated with the state-edge system. In particular, pointers themselves should be just integers.
    \item The intermediate representation should have a high-quality API for creating and consuming in-memory IR.
    \item Programs should be able to be richly annotated with metadata, including optimization information.
\end{itemize}
Our current design replaces the RVSDG's use of \(\theta\)-nodes and \(\gamma\)-nodes for control flow with mutually recursive basic blocks with arguments to make it easier to interpret the IR code as a functional program as in \textit{SSA is Functional Programming} \cite{ssa-functional}.

\TODO{Use \(\theta\)-node/\(\gamma\)-node or theta-node/gamma-node? And is \(\beta\)-node a good name for the basic-block construct?}

\TODO{rest}

\section{Isotope}

The \tty{isotope} intermediate representation is, in essence, \tty{isotope-ir} refined using the logic of \tty{isotope-core}. In particular, compilation to \tty{isotope-ir} should be achievable using a simple "forgetful-functor" like transformation. 

\TODO{rest}

\chapter{Initial Work}

\TODO{this}

\chapter{Future Work}

\TODO{this}

\newpage

\addcontentsline{toc}{chapter}{Bibliography}
\printbibliography

\end{document}